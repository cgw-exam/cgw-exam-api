<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use Storage;

class Job extends Model
{
    protected $fillable = ['title', 'description', 'date', 'location'];

    protected $with = ['applicants'];

    public function applicants(){
        return $this->belongsToMany('\App\Applicant')->withTimestamps();
    }
}
