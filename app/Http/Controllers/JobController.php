<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Storage;
use Str;
use \App\Job;
use \App\Applicant;

class JobController extends Controller
{
    public function getData(){
        $jobs_data = Storage::disk('local')->path('jobs.csv');
        $file = fopen($jobs_data, 'r');
        $jobs = collect();

        while( ($csv_record = fgetcsv($file, 200, ",")) != false) {
            $job = collect([
                'title' => e($csv_record[0]),
                'description' => e($csv_record[1]),
                'date'=>e($csv_record[2]),
                'location'=>e($csv_record[3]),
                'applicants'=> Str::of(e($csv_record[4]))->split("/[\s,]+/")
            ]);

            $jobs->push($job);
        }

        // removing header
        $jobs->shift();

        foreach($jobs as $job){
            
            $new_job = Job::create([
                'title' => $job['title'],
                'description' => $job['description'],
                'date' => $job['date'],
                'location' => $job['location']
            ]);

            foreach($job['applicants'] as $applicant){
                $new_applicant = Applicant::firstOrCreate(['name' => $applicant]);

                $new_job->applicants()->attach($new_applicant->id);
            }
        }

        return 'Successfully imported csv data to database';
    }

    public function index(){
        $jobs = Job::all();

        return $jobs->toJson();
    }
}
