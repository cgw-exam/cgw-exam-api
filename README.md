# Job List Backend API
A simple laravel backend app.

## Installation

Clone this project <foobar>
```bash
git clone foobar
```

Then install the dependencies:
```bash
composer install
```

To use the app locally:
Create a mysql database locally.

Create a .env file in the root directory.
Fill-out the database details:
Here
DB_CONNECTION=mysql
DB_HOST=<your host>
DB_PORT=3306
DB_DATABASE=<database name>
DB_USERNAME=<database username>
DB_PASSWORD=<datanase password>

## Usage

Migrate the database
```bash
php artisan migrate
```

Serve the project
```bash
php artisan serve
```

To initially load the data from the csv file:
go to /api/get-data

After loading the data, to see the list of jobs:
/api/jobs
